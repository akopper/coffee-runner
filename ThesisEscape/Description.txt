THESIS ESCAPE

Everyone of us knows this feeling. We're lying in our bed trying to fall asleep, but this unfinished master thesis sits on your mind. Then in then middle of the night you feel an uneasy presence right behind you. Just as you turn around you see this monster forming from your unfinished work. The first thing that comes to your mind is start running as fast as you can. But you're tired as hell. Will you escape this nightmare while trying to not fall asleep? Test how far you can get.
You retrieve energy by collecting cups of coffee and pages for your paper. As awesome as beer and other mind-expanding substances may be for you, they will make you sleepy, so try to avoid them.

Thanks to everyone who contributed to this game! 

Team
Alexander Kopper
Johannes Wagner
Stefan Stattmann
Verena Dittmer

Art by
Natascha Sobe
Martin Kogler
Jürgen Fischer

Music by
Mathias Lux

Sounds
http://soundbible.com/1502-Slurping-2.html
http://www.freesound.org/people/bsmacbride/sounds/108519/
http://www.freesound.org/people/Hephaestus/sounds/250140/
http://www.freesound.org/people/thecheeseman/sounds/44428/
http://www.freesound.org/people/thecheeseman/sounds/44429/
http://www.freesound.org/people/thecheeseman/sounds/44430/
http://www.freesound.org/people/smcameron/sounds/50801/
http://www.freesound.org/people/DJ%20Chronos/sounds/130282/
www.freesound.org/people/stijn/sounds/43673/download/43673__stijn__pencil1.wav

