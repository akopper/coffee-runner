﻿using UnityEngine;
using System.Collections;

public class TitlescreenLogic : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Application.targetFrameRate = 1000;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LoadMainLevel() {
		Application.LoadLevel ("Intro");
	}

	public void Shutdown() {
		Application.Quit ();
	}

	public void LoadCredits() {
		Application.LoadLevel ("Credits");
	}

}
