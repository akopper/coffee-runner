﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HighscoreGUI {
 

	public HighscoreGUI() {
		this.playerIsAllowedToEnterHighscore = true;
	}

	public HighscoreGUI (int score, bool canEnterHighscore, string HighscorePrefix) {

		this.Score = score;
		this.playerIsAllowedToEnterHighscore = canEnterHighscore;
		prefix = HighscorePrefix;
	}

	private List<HighscoreEntry> Highscores;
	public string HighscoreNameOfPlayer = "";
	public int Score;
	private string prefix = "Default_";

	public bool playerIsAllowedToEnterHighscore = false;


	private const int ScoreXOffset = 250;
	private const int ScoreYOffset = 24;

	public List<HighscoreEntry> GetHighscores() {
		LoadHighscoresIfNotLoaded ();
		return Highscores;
	}

	void LoadHighscoresIfNotLoaded ()
	{
		if (Highscores == null) {
			LoadHighscores (prefix);
		}
	}

	private void LoadHighscores(string prefix) {
		Highscores = new List<HighscoreEntry>();
		for(int i = 0; i < 999; i ++){
			var currentHighscore = new HighscoreEntry();
			if(PlayerPrefs.HasKey(prefix+i+"HScore") && PlayerPrefs.HasKey(prefix + i + "HScoreName")){
				currentHighscore.Name = PlayerPrefs.GetString(prefix + i + "HScoreName");
				currentHighscore.Score = PlayerPrefs.GetInt(prefix + i + "HScore");
				Highscores.Insert(i, currentHighscore);
			} else {
				// stop loading scores as soon as one index misses
				break;
			}
		}
	}

	private void SaveHighscore(string prefix) {
		int counter = 0;
		foreach(HighscoreEntry entry in Highscores){
			PlayerPrefs.SetInt(prefix + counter + "HScore", entry.Score);
			PlayerPrefs.SetString(prefix + counter + "HScoreName", entry.Name);
			counter ++;
		}
	}

	public int GetPlaceInHighscore(int score) {
		LoadHighscoresIfNotLoaded ();
		for(int counter = 0; counter < Highscores.Count; counter ++){
			var entry = Highscores[counter];
			if(entry.Score <= score) {
				return counter + 1;
			}
		}
		return Highscores.Count+1;
	}

	private void UnloadHighscores() {
		Highscores = null;
	}

	public void AddScore() {
		if(!CanEnterHighscore()) {
			Debug.LogWarning("No Entry allowed");
			return;
		}
		AddScore(HighscoreNameOfPlayer, Score);
	}

	public void AddScore(string name, int score) {
		if(!CanEnterHighscore()) {
			Debug.LogWarning("No Entry allowed");
			return;
		}
		var HighscoreToAdd = new HighscoreEntry(name, score);
		AddScore (HighscoreToAdd);
	}

	public void AddScore(HighscoreEntry highscoreToAdd){
		if(!CanEnterHighscore()) {
			Debug.LogWarning("No Entry allowed");
			return;
		}
		LoadHighscoresIfNotLoaded ();
		Score = highscoreToAdd.Score;
		SaveLastPlayerName(highscoreToAdd.Name);
		if(Highscores.Count == 0){
			Highscores.Insert(0, highscoreToAdd);
		} else {
			bool added = false;
			for(int counter = 0; counter < Highscores.Count; counter ++){
				var entry = Highscores[counter];
				if(entry.Score <= highscoreToAdd.Score) {
					Highscores.Insert(counter, highscoreToAdd);
					added = true;
				    break;
				}
			}
			// if player is last, add to end
			if(!added){
				Highscores.Add(highscoreToAdd);
			}
		}
        SaveHighscore(prefix);
        DisallowEntry();
	    HighscoreNameOfPlayer = "";
	}
	
	public void AllowEntry() {
		playerIsAllowedToEnterHighscore = true;
	}

	public void DisallowEntry() {
		playerIsAllowedToEnterHighscore = false;
	}

	public bool CanEnterHighscore() {
		return playerIsAllowedToEnterHighscore;
	}

	public string GetLastPlayerName() {
		return PlayerPrefs.GetString(prefix + "LastPlayerName");
	}

	private void SaveLastPlayerName(string playerName) {
		PlayerPrefs.SetString(prefix + "LastPlayerName", playerName);
	}



}

public class HighscoreEntry {
	public int Score { get; set; }
	public string Name { get; set; }

	public HighscoreEntry() {}

	public HighscoreEntry(string name, int score) {
		Score = score;
		Name = name;
	}
}