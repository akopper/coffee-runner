﻿using UnityEngine;
using System.Collections;

public class DataExchange : MonoBehaviour {


	public int Score { get; set; }
	public int speedUpCounter = 0;

	// Use this for initialization
	void Start () {
		Reset ();
	}

	public void addToScore(int points) {
		Score += points;
		if ((Score / 100) > speedUpCounter) {
			Grid.EventHub.TriggerSpeedUpMovement();
			speedUpCounter++;
		}
	}

	public void Reset() {
		Score = 0;
	}

}
