﻿using UnityEngine;
using System.Collections;

public class DelayedLevelLoad : MonoBehaviour {

	public float DelayTime;
	public string LevelNameToLoad;
	public bool AllowSkip;

	private float timePassed;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Jump") || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)){
			LoadMainLevel();
		}
		timePassed += Time.deltaTime;
		if (timePassed <= DelayTime) {
			return;
		}
		LoadMainLevel();

	}

	private void LoadMainLevel() {
		Application.LoadLevel (LevelNameToLoad);
	}
}
