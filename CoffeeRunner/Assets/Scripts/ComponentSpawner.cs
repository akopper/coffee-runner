﻿using UnityEngine;
using System.Collections;

public class ComponentSpawner : MonoBehaviour {

	public GameObject[] PrefabsToSpawn;

	public string MethodToCall;
	public string ObjectToCallMethodOn;

	void Awake () {
		foreach(GameObject prefab in PrefabsToSpawn) {
			if(prefab == null) continue;

			var go = GameObject.Find (prefab.name);
			if (go == null) {
				// spawn neccessary objects
				var instance = Instantiate(prefab);
				instance.name = prefab.name;
			} else {
				Debug.Log(string.Format("Found '{0}' in scene, not spawning", prefab.name));
			}
		}
		if(!string.IsNullOrEmpty(MethodToCall) && !string.IsNullOrEmpty(ObjectToCallMethodOn)){
			GameObject receiver = GameObject.Find(ObjectToCallMethodOn);
			receiver.SendMessage(MethodToCall);
		}
	}

}
