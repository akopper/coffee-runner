﻿using UnityEngine;
using System.Collections;

public class EventHub : MonoBehaviour {

	#region Event delegates
	public delegate void VoidEvent();
	public delegate void IntegerParamEvent (int value);
	public delegate void StringParamEvent (string value);
	#endregion


	#region
	public event VoidEvent LevelStart;
	public event VoidEvent LevelEnd;
	public event VoidEvent JumpAnimation;
	public event VoidEvent JumpAnimationEnd;
	public event VoidEvent SpeedUpMovement;
	public event IntegerParamEvent GameOver;
	public event StringParamEvent ItemPickup;
	public event StringParamEvent ObstacleCollision;

	#region Triggers
	public void TriggerLevelStart(){
		if (LevelStart != null) {
			LevelStart ();
		}
	}
	public void TriggerLevelEnd(){
		if (LevelEnd != null) {
			LevelEnd ();
		}
	}
	public void TriggerJumpAnimation() {
		if (JumpAnimation != null) {
			JumpAnimation ();
		}
	}
	public void TriggerJumpAnimationEnd() {
		if (JumpAnimationEnd != null) {
			JumpAnimationEnd ();
		}
	}
	public void TriggerSpeedUpMovement() {
		if (SpeedUpMovement != null) {
			SpeedUpMovement ();
		}
	}
	public void TriggerItemPickup(string name) {
		if (ItemPickup != null) {
			ItemPickup (name);
		}
	}
	public void TriggerGameOver(int time) {
		if (GameOver != null) {
			GameOver (time);
		}
	}
	public void TriggerObstacleCollision(string name) {
		if (ObstacleCollision != null) {
			ObstacleCollision (name);
		}
	}
	#endregion
#endregion



}
