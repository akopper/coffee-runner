﻿using UnityEngine;
using System.Collections;

public class TitlescreenItemMovement : MonoBehaviour {

	public float force = 20;
	public float minVelocity = 1;

	private Rigidbody2D body;

	// Use this for initialization
	void Start () {
		Debug.Log ("Adding force");
		body = GetComponent<Rigidbody2D> (); 
		body.AddForce (new Vector2(Random.Range(-10.0F, 10.0F), Random.Range(-10.0F, 10.0F)) * force);
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 vector = body.velocity;

		// Mean square root
		if (Mathf.Sqrt (vector.x * vector.x + vector.y * vector.y) < minVelocity) {
			body.velocity = new Vector2 (vector.x * 1.5f, vector.y * 1.5f);
		}
	
	}
}
