﻿using UnityEngine;
using System.Collections;


// Here comes the behaviour of the player
// calls to animations
// 
public class Student : MonoBehaviour {

	public int energy; 
	public AudioClip jumpSound;
	public AudioClip[] collisionSounds;

	public AudioClip sleepSound;
	public AudioClip omnomnomSound;

	private static readonly int PICK_UPS_LAYER = 8;
	private static readonly int MASTER_THESIS_LAYER = 10;

	private Rigidbody2D body;
	private Animator animator;


	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator> ();
		Grid.EventHub.ObstacleCollision += playCollisionSound;
		Grid.EventHub.GameOver += GameOverAnimation;
		Grid.EventHub.JumpAnimation += animateJump;
		Grid.EventHub.JumpAnimationEnd += disAnimaiteJump;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void playJumpSound() {
		Grid.SoundManager.RandomizeSfx(0.5f, jumpSound);
	}

	public void playCollisionSound(string objectName) {
		Grid.SoundManager.RandomizeSfx (1, collisionSounds);
	}

	public void animateJump() {
		animator.SetBool ("jumping", true);
		playJumpSound ();
	}
	
	public void disAnimaiteJump() {
		animator.SetBool("jumping", false);
	}

	public void GameOverAnimation(int time) {
		//todo: sleepanimation
		if (time < 2) {
			Grid.SoundManager.PlaySingle (omnomnomSound, 1);
		} else {
			Grid.SoundManager.PlaySingle (sleepSound, 1);
			animator.SetTrigger ("sleeping");
		}
	}

	void OnTriggerEnter2D(Collider2D collision) {
		if (collision.gameObject.layer == PICK_UPS_LAYER) {
			Debug.Log("Collides with pick ups");
		}
		if (collision.gameObject.layer == MASTER_THESIS_LAYER) {
			Debug.Log("Collides with Thesis");
			Grid.EventHub.TriggerGameOver(0);
		}
	}
}
