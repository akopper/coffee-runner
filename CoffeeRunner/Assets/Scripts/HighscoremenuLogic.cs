﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighscoremenuLogic : MonoBehaviour {

	private HighscoreGUI highscoreManagement;
	private int place;

	public GameObject GameEntriesTextfield;
	public GameObject YourScore;
	public GameObject NameField;
	public GameObject SaveButtonGO;

	private Text HighscoreEntriesTextView;
	private Text YourScoreText;
	private InputField NameFieldComp;
	private Button SaveButton;
	// Use this for initialization
	void Start () {
		highscoreManagement = new HighscoreGUI ();
		highscoreManagement.AllowEntry ();

//		place = highscoreManagement.GetPlaceInHighscore (Grid.Memory.Score);

		HighscoreEntriesTextView = GameEntriesTextfield.GetComponent<Text> ();
		YourScoreText = YourScore.GetComponent<Text> ();
		NameFieldComp = NameField.GetComponent<InputField> ();
		SaveButton = SaveButtonGO.GetComponent<Button> ();

		UpdateHighscoreEntries ();
		YourScoreText.text = "Your Score: " + (Grid.DataExchange == null ? 00000 : Grid.DataExchange.Score);
		NameFieldComp.text = highscoreManagement.GetLastPlayerName();
	}
	
	// Update is called once per frame
	void Update () {

	}


	public void BackToMain() {
		Application.LoadLevel ("TitleScreen");
	}

	public void StartGame() {
		Application.LoadLevel ("Level1");
	}

	public void SaveHighscore(GameObject nameTextField) {
		int score = Grid.DataExchange.Score;
		string playerName = nameTextField.GetComponent<Text> ().text;
		Debug.Log(string.Format("name: {0}, score: {1}", score, playerName));
		highscoreManagement.AddScore(new HighscoreEntry(playerName, score));
		highscoreManagement.DisallowEntry ();
		UpdateHighscoreEntries ();
		SaveButton.interactable = false;
	}

	private void UpdateHighscoreEntries() {
		var hscores = highscoreManagement.GetHighscores ();
		string highscores = "";
		foreach(HighscoreEntry entry in hscores) {
			highscores += string.Format("{0,-10} {1}\n", entry.Score, entry.Name);
		}
		HighscoreEntriesTextView.text = highscores;
	}

}
