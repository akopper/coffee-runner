﻿using UnityEngine;
using System.Collections;

public class WorldGenerator : MonoBehaviour {
	public GameObject[] Platforms;
	public GameObject[] Abysses;
	public GameObject[] Obstacles;
	public GameObject[]	Pickups;
	public GameObject[] ScorePickups;
	public int pickupProbability = 60;
	public int scorePickupProbability = 70;
	public int abyssProbability = 70;
	public int abyssMinRange = 2;
	public int abyssMaxRange = 6;
	public float itemMaxHeight = 3f;

	private float offset = 0f;
	private bool platformNow = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate() {
		RaycastHit2D hit = Physics2D.Raycast (transform.position, -Vector2.up);

		if (hit.collider == null) {
			if (transform.position.x >= offset) {
				int diceRoll = Random.Range(0, 101);
				if (diceRoll >= abyssProbability || platformNow) {
					spawnTheRandomnes(false);
					platformNow = false;
				} else {
					int xmod = Random.Range(abyssMinRange,abyssMaxRange);
					offset = transform.position.x + xmod;
					spawnTheRandomnes(true);
					platformNow = true;
				}
			}
		}
	}

	void spawnTheRandomnes(bool makeAbyss) {
		int diceRoll = Random.Range(0, 101);
		int randPlatform = Random.Range(0, Platforms.Length);
		int randAbyss = Random.Range(0, Abysses.Length);
		int randThing = 0;

		int xmod = Random.Range(0,8);
			
		float posx = transform.position.x + 2;
		float itemYOffset = Random.Range (0.5f, itemMaxHeight);

		if (!makeAbyss) {
			GameObject newPlatform = (GameObject)Instantiate (Platforms [randPlatform], new Vector3 (posx, 0, 0), Quaternion.identity);
		} else {
			GameObject newAbyss = (GameObject)Instantiate (Abysses [randAbyss], new Vector3 (posx-3, 0, 0), Quaternion.identity);
		}
		GameObject itemToSpawn;


		if (diceRoll <= pickupProbability) {
			if (diceRoll <= scorePickupProbability) {
				randThing = Random.Range (0, ScorePickups.Length);
				itemToSpawn = ScorePickups[randThing];
			} else {
				randThing = Random.Range (0, Pickups.Length);
				itemToSpawn = Pickups[randThing];
			}
			GameObject newThing = (GameObject)Instantiate(itemToSpawn, new Vector3(posx + xmod,itemYOffset,0f), Quaternion.identity);
		} else {
			randThing = Random.Range (0, Obstacles.Length);
			GameObject newThing = (GameObject)Instantiate(Obstacles[randThing], new Vector3(posx + xmod,0.5f,0f), Quaternion.identity);
		}

		//Debug.Log ("RANDOOM");
	}
}
