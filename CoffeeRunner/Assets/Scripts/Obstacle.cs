﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {
	public string callScript;
	public AudioClip collisionSound;

	private static readonly int MASTER_THESIS_LAYER = 10;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.layer != MASTER_THESIS_LAYER) {
			Grid.EventHub.TriggerObstacleCollision (callScript);
			Grid.SoundManager.PlaySingle (collisionSound);
		}
		Destroy(gameObject);
	}
}
