﻿using UnityEngine;
using System.Collections;

public class SoundTest : MonoBehaviour {

	public AudioClip jumpSound;
	public AudioClip pickupCoffeeSound;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Jump")) {
			Grid.SoundManager.PlaySingle(jumpSound,0.1f);
		}
		if (Input.GetButtonDown ("Fire1")) {
			Grid.SoundManager.RandomizeSfx(1f, pickupCoffeeSound);
		}
	}
}
