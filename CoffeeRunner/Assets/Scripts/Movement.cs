﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public float jumpForce;
	public float velocity;
	public float speedupScoreDevider = 200;
	public float startMovementDelay;
	public bool ignoreObstacles = false;
	public LayerMask obstacleLayer;

	private int jumpPossible = 2; 
	private bool canJump = false;
	private bool canMove = true;
	private Rigidbody2D body;


	private float time = 0;

	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody2D> ();
		Grid.EventHub.GameOver += stopMovement;
		Grid.EventHub.ObstacleCollision += HandleObstacleCollision;
	}

	public void HandleObstacleCollision (string value)
	{
		if (!ignoreObstacles) {
			/*canMove = false;
			body.AddForce (new Vector2 (-1f, 0.1f) * (180f * (1 + (Grid.DataExchange.Score / speedupScoreDevider))));
			canMove = true;*/
			body.AddForce (-Vector2.right * (120f * (1 + (Grid.DataExchange.Score / speedupScoreDevider / 4f))));
		}
	}
	
	// Update is called once per frame
	void Update () {

		time += Time.deltaTime;
		if (time <= startMovementDelay || !canMove)
			return;
		
		transform.Translate((velocity + (Grid.DataExchange.Score / speedupScoreDevider)) * Time.smoothDeltaTime, 0f, 0f);

		//#if UNITY_STANDALONE
		if (Input.GetButtonDown ("Jump") && jumpPossible > 0) {
			canJump = true;
			jumpPossible -= 1;
		}
		//#elif (UNITY_IOS || UNITY_ANDROID)
		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began && jumpPossible > 0) {
			canJump = true;
			jumpPossible -= 1;
		}
		//#endif
	}

	public void stopMovement(int time) {
		if (!ignoreObstacles) {
			canMove = false;
		}
	}

	// Update for Rigidbody and 2D
	void FixedUpdate() {


		RaycastHit2D hit = Physics2D.Raycast (transform.position, -Vector2.up);

		if (canJump && (hit.collider != null)) {
			canJump = false;
			Grid.EventHub.TriggerJumpAnimation();
			body.AddForce (new Vector2 (0, 1) * jumpForce);
		}
	}


	void OnCollisionEnter2D(Collision2D collision) {
		jumpPossible = 2;
		Grid.EventHub.TriggerJumpAnimationEnd ();
	}	

}
