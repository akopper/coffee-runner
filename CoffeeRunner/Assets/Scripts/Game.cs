﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


// GameLogic controlling business logic
// loading scenes
// 
public class Game : MonoBehaviour {
	public float maxEnergy = 100;
	public float currentEnergy = 100;
	public int runningEnergyModifier=10;
	public int StartDelay = 3;
	public int PointsEarnedPerSecond = 5;

	private bool timercoroutineStarted = false;

	private float timePassed = 0;
	private Dictionary<string, GameObject> childSkripts;
	private bool gameOver = false;

	// Use this for initialization
	void Start () {
		childSkripts = new Dictionary<string, GameObject> ();
		foreach (Transform child in transform) {
			childSkripts.Add(child.gameObject.name,child.gameObject);
		}
		Grid.EventHub.ItemPickup += sendToChild;
		Grid.EventHub.ObstacleCollision += sendToChild;
		Grid.EventHub.GameOver += GameOver;

		InvokeRepeating("IncreaseScorePerSecond", StartDelay, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		timePassed += Time.deltaTime;
		if (timePassed <= StartDelay)
			return;

		decreseEnergy (runningEnergyModifier * Time.deltaTime);
	}

	public void sendToChild (string name) {
		childSkripts[name].SendMessage("handle");
	}

	public GameObject getChildObject (string name) {
		return childSkripts [name];
	}

	public void increaseEnergy(float value) {
		currentEnergy = ((currentEnergy + value) < maxEnergy) ? currentEnergy + value : maxEnergy;
	}

	public void decreseEnergy(float value) {
		if (currentEnergy - value > 0) {
			currentEnergy -= value;
		} else {
			currentEnergy = 0;
			Grid.EventHub.TriggerGameOver(5);
		}
	}

	private void IncreaseScorePerSecond() {

			Grid.DataExchange.addToScore(PointsEarnedPerSecond);
	
	}

	public void GameOver(int time) {
		// cancel all invoke calls on this mbehaviour
		CancelInvoke();
		Invoke("LoadHighscore",time);
	}

	public void LoadHighscore() {
		LoadScene("Highscore");
	}

	public void LoadScene(string name) {
		// do some stuff that has to be done. opt.
		// load the scene by name
		Application.LoadLevel (name);
	}

	public void ExitGame() {

	}

}
