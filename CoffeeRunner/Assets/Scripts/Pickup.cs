﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {
	public string callScript;
	public AudioClip pickupSound;

	private static readonly int MASTER_THESIS_LAYER = 10;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.layer != MASTER_THESIS_LAYER) {
			Grid.EventHub.TriggerItemPickup (callScript);
			Grid.SoundManager.PlaySingle (pickupSound);
			Destroy (gameObject);
		}
	}
}
