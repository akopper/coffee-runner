﻿using UnityEngine;
using System.Collections;

public class handlePaper : MonoBehaviour {
	public float energyBonus = 20;
	public int scoreValue = 50;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void handle() {
		Grid.GameLogic.increaseEnergy (energyBonus);
		Grid.DataExchange.addToScore(scoreValue);
	}
}
