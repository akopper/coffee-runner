﻿using UnityEngine;
using System.Collections;

public class handleCoffee : MonoBehaviour {
	public float energyBoost = 10;
	public int scoreValue = 30;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void handle() {
		Grid.GameLogic.increaseEnergy (energyBoost); 
		Grid.DataExchange.addToScore(scoreValue);
	}
}
