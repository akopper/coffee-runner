﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GameUILogic : MonoBehaviour {

	public GameObject ScoreGO;
	public GameObject EnergyBarGO;
	
	private Text scoreText;
	private Image energyBar;

	// Use this for initialization
	void Start () {
		scoreText = ScoreGO.GetComponent<Text> ();
		energyBar = EnergyBarGO.GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {
		// set score
		scoreText.text = string.Format("{0}", Grid.DataExchange.Score); 
		energyBar.fillAmount = Grid.GameLogic.currentEnergy / Grid.GameLogic.maxEnergy;
	}



}
